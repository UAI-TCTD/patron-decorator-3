﻿Public Class CafeSolo
    Inherits Bebida


    Public Overrides Function Costo() As Double
        Return 10
    End Function

    Public Overrides ReadOnly Property GetDescripcion As Object
        Get
            Return "Café sólo"
        End Get
    End Property
End Class
