﻿Public Class Edulcorante
    Inherits AgregadoDecorator
    Private _bebida As Bebida
    Public Sub New(bebida As Bebida)
        _bebida = bebida

    End Sub
    Public Overrides Function Costo() As Double
        Return _bebida.Costo + 1
    End Function

    Public Overrides ReadOnly Property getDescripcion As Object
        Get
            Return _bebida.GetDescripcion & ", Edulcorante"
        End Get
    End Property
End Class
