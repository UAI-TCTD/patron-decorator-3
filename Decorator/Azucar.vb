﻿Public Class Azucar
    Inherits AgregadoDecorator
    Private _bebida As Bebida
    Public Sub New(bebida As Bebida)
        _bebida = bebida

    End Sub
    Public Overrides Function Costo() As Double
        Return _bebida.Costo + 0.5
    End Function

    Public Overrides ReadOnly Property getDescripcion As Object
        Get
            Return _bebida.GetDescripcion & ", Azucar"
        End Get
    End Property
End Class
